package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextConfirmPassword: EditText
    private lateinit var buttonRegistration: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()
        registerListeners()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword)
        buttonRegistration = findViewById(R.id.buttonRegistration)

    }
    private fun registerListeners (){
        buttonRegistration.setOnClickListener{
            val email=editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if(email.isEmpty()||password.isEmpty()){
                Toast.makeText(this, "empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password.length < 9) {
                Toast.makeText(this, "Minimum 9 character password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "Minimum one upper-case character in password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[a-z].*".toRegex())) {
                Toast.makeText(this, "Minimum one lower-case character in password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "Minimum one number in password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (editTextConfirmPassword.text.toString() != editTextPassword.text.toString()) {
                Toast.makeText(this, "Invalid confirm password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        startActivity(Intent(this, ProfileActivity::class.java) )
                        finish()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }


                }

        }

    }

}